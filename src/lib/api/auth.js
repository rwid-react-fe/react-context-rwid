import { faker } from "@faker-js/faker";

export const login = () => {
  const user = {
    email: faker.internet.email(),
    username: faker.internet.userName(),
    fullname: faker.person.fullName(),
    wallet: {
      address: faker.finance.ethereumAddress(),
      value: faker.finance.amount({
        symbol: "$",
      }),
    },
    hoursSpent: faker.number.int({ min: 10, max: 9000 }),
  };

  return {
    data: {
      user,
    },
  };
};
