import localforage from "localforage";

const fitness = localforage.createInstance({
  name: "fitness",
});

const user = localforage.createInstance({
  name: "user",
});

const db = {
  fitness,
  user,
};

export { db };
