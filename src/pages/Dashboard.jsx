import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
import Navbar from "../components/Navbar/Navbar";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

const Dashboard = () => {
  // Placeholder data for the chart
  const chartData = {
    labels: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    datasets: [
      {
        label: "Minutes Spent",
        data: [10, 150, 200, 180, 250, 10, 150],
        fill: false,
        borderColor: "rgba(75,192,192,1)",
      },
    ],
  };

  return (
    <div>
      <Navbar />
      <div className="flex">
        {/* Sidebar */}
        <aside className="bg-blue-700 text-white w-1/4 p-4 min-h-screen">
          <h2 className="text-2xl font-bold mb-4">Dashboard</h2>
          <ul>
            <li className="mb-2">
              <a href="#" className="hover:text-gray-300">
                Home
              </a>
            </li>
            <li className="mb-2">
              <a href="#" className="hover:text-gray-300">
                Wallet
              </a>
            </li>
            <li className="mb-2">
              <a href="#" className="hover:text-gray-300">
                Locker
              </a>
            </li>
          </ul>
        </aside>

        {/* Content */}
        <section className="w-3/4 p-8 space-y-6">
          <h1 className="text-4xl font-bold underline decoration-sky-400">Hi, John Doe!</h1>
          <ChartCard title="Minutes Spent">
            <Line
              data={chartData}
              height={500}
              options={{ maintainAspectRatio: false }}
            />
          </ChartCard>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            <StatCard title="Wallet" value="$500" />
            <StatCard title="Hours Spent" value="30 hours" />
          </div>
        </section>
      </div>
    </div>
  );
};

const ChartCard = ({ title, children }) => {
  return (
    <div className="bg-white rounded-lg overflow-hidden shadow-md p-6">
      <h3 className="text-lg font-semibold mb-4">{title}</h3>
      <div className="flex justify-center">{children}</div>
    </div>
  );
};

const StatCard = ({ title, value }) => {
  return (
    <div className="bg-white rounded-lg overflow-hidden shadow-md p-6">
      <h3 className="text-lg font-semibold mb-2">{title}</h3>
      <p className="text-3xl text-blue-700">{value}</p>
    </div>
  );
};

export default Dashboard;
