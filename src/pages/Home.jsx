import Navbar from "../components/Navbar/Navbar";

const Home = () => {
  return (
    <div>
      <Navbar />
      <header className="bg-blue-700 text-white text-center py-16">
        <h1 className="text-4xl font-bold">Your Fitness Journey Starts Here</h1>
        <p className="mt-4">
          Achieve your fitness goals with our personalized plans and expert
          guidance.
        </p>
      </header>

      <section className="bg-gray-100 py-16">
        <div className="container mx-auto text-center">
          <h2 className="text-2xl font-bold mb-4">Key Features</h2>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            <FeatureCard
              title="Personalized Workouts"
              description="Get custom workout plans tailored to your fitness level and goals."
            />
            <FeatureCard
              title="Nutrition Guidance"
              description="Explore a variety of healthy meal options designed to support your fitness journey."
            />
            <FeatureCard
              title="Progress Tracking"
              description="Track your workouts, monitor progress, and stay motivated on your fitness path."
            />
          </div>
        </div>
      </section>

      <section className="py-16">
        <div className="container mx-auto text-center">
          <h2 className="text-2xl font-bold mb-4">What Our Users Say</h2>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            <TestimonialCard
              name="John Doe"
              quote="I achieved my fitness goals faster with the help of the Fitness App. Highly recommended!"
            />
            <TestimonialCard
              name="Jane Smith"
              quote="The personalized workouts and nutrition advice made a significant difference in my fitness journey."
            />
            {/* Add more testimonials */}
          </div>
        </div>
      </section>

      <section className="bg-gray-100 py-16">
        <div className="container mx-auto text-center">
          <h2 className="text-2xl font-bold mb-4">Pricing Plans</h2>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            <PricingCard
              title="Basic"
              price="$9.99/month"
              features={[
                "Access to personalized workouts",
                "Nutrition guidance",
                "Progress tracking",
              ]}
            />
            <PricingCard
              title="Standard"
              price="$19.99/month"
              features={[
                "All Basic features",
                "Additional workout plans",
                "Priority support",
              ]}
            />
            <PricingCard
              title="Premium"
              price="$29.99/month"
              features={[
                "All Standard features",
                "Exclusive access to live sessions",
                "Dietitian consultation",
              ]}
            />
          </div>
        </div>
      </section>

      <section className="py-16 bg-blue-700 text-white">
        <div className="container mx-auto text-center">
          <h2 className="text-2xl font-bold mb-4">
            Start Your Fitness Journey Today
          </h2>
          <p className="mb-8">
            Join us now and unlock a healthier, happier you!
          </p>
          <button className="bg-white text-blue-700 py-2 px-6 rounded-full">
            Get Started
          </button>
        </div>
      </section>

      <section className="py-16">
        <div className="container mx-auto text-center">
          <h2 className="text-2xl font-bold mb-4">Contact Us</h2>
          <p className="mb-8">
            Have questions or need assistance? Reach out to our support team.
          </p>
          <p>Email: support@fitnessapp.com</p>
          <p>Phone: 123-456-7890</p>
        </div>
      </section>
    </div>
  );
};

const FeatureCard = ({ title, description }) => {
  return (
    <div className="bg-white rounded-lg overflow-hidden shadow-md p-6">
      <h3 className="text-lg font-semibold mb-2">{title}</h3>
      <p className="text-gray-600">{description}</p>
    </div>
  );
};

const TestimonialCard = ({ name, quote }) => {
  return (
    <div className="bg-white rounded-lg overflow-hidden shadow-md p-6">
      <p className="text-gray-600">{quote}</p>
      <p className="text-gray-800 font-semibold mt-4">{name}</p>
    </div>
  );
};

const PricingCard = ({ title, price, features }) => {
  return (
    <div className="bg-white rounded-lg overflow-hidden shadow-md p-6">
      <h3 className="text-lg font-semibold mb-2">{title}</h3>
      <p className="text-gray-600">{price}</p>
      <ul className="mt-4">
        {features.map((feature, index) => (
          <li key={index} className="text-gray-600">
            {feature}
          </li>
        ))}
      </ul>
    </div>
  );
};



export default Home;
